<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\ApiController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::post('login', 'App\Http\Controllers\ApiController@login');
Route::post('register', 'App\Http\Controllers\ApiController@register');

Route::group(['middleware' => ['jwt.verify']], function() {
    Route::get('data', 'App\Http\Controllers\ApiController@getData');
    Route::get('get_user', 'App\Http\Controllers\ApiController@get_user');

    Route::get('folder', 'App\Http\Controllers\FolderController@index');
    Route::post('folder', 'App\Http\Controllers\FolderController@store');
    Route::get('folder/{folder}', 'App\Http\Controllers\FolderController@show');
    Route::delete('folder/{folder}', 'App\Http\Controllers\FolderController@destroy');

    Route::get('document', 'App\Http\Controllers\DocumentController@index');
    Route::post('document', 'App\Http\Controllers\DocumentController@store');
    Route::get('document/{document}', 'App\Http\Controllers\DocumentController@show');
    Route::delete('document/{document}', 'App\Http\Controllers\DocumentController@destroy');
});
