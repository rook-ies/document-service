<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Jenssegers\Mongodb\Eloquent\Model as Eloquent;
use Illuminate\Auth\Authenticatable as AuthenticatableTrait;
use Illuminate\Contracts\Auth\Authenticatable;
use Tymon\JWTAuth\Contracts\JWTSubject;

class Document extends eloquent implements Authenticatable
{
    use AuthenticatableTrait;

    protected $connection = 'mongodb';
    protected $collections = 'document';

    protected $fillable = [
        '_id','name', 'type', 'folder_id','content', 'owner_id','share','company_id', 'timestamp',
    ];

    protected $casts = [
      'content' => 'array',
    ];
}
