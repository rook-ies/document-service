<?php

namespace App\Http\Controllers;

// use JWTAuth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Models\User;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class ApiController extends Controller
{
  public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login','register']]);
    }

    public function register(Request $request)
      {
          $data = $request->only('name', 'company_id', 'password');
          $validator = Validator::make($data, [
              'name' => 'required|string',
              'password' => 'required|string|min:6|max:50'
          ]);

          if ($validator->fails()) {
              return response()->json(['error' => $validator->messages()], 200);
          }

          $user = User::create([
          	'name' => $request->name,
          	'company_id' => $request->company_id,
          	'password' => bcrypt($request->password)
          ]);

          return response()->json([
              'success' => true,
              'message' => 'User created successfully',
              'data' => $user
          ], Response::HTTP_OK);
      }

      public function login(Request $request)
      {
        $credentials = $request->only('name', 'password');

        //valid credential
        $validator = Validator::make($credentials, [
            'name' => 'required|string',
            'password' => 'required|string|min:6|max:50'
        ]);

        //Send failed response if request is not valid
        if ($validator->fails()) {
            return response()->json(['error' => $validator->messages()], 200);
        }

        //Request is validated
        //Crean token
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return response()->json([
                	'success' => false,
                	'message' => 'Login credentials are invalid.',
                ], 400);
            }
        } catch (JWTException $e) {
    	return $credentials;
            return response()->json([
                	'success' => false,
                	'message' => 'Could not create token.',
                ], 500);
        }

 		//Token created, return with success response and jwt token
        return response()->json([
            'success' => true,
            'token' => $token,
        ]);
      }

      public function logout(Request $request)
      {
          $validator = Validator::make($request->only('token'), [
              'token' => 'required'
          ]);

          if ($validator->fails()) {
              return response()->json(['error' => $validator->messages()], 200);
          }

          try {
              JWTAuth::invalidate($request->token);

              return response()->json([
                  'success' => true,
                  'message' => 'User has been logged out'
              ]);
          } catch (JWTException $exception) {
              return response()->json([
                  'success' => false,
                  'message' => 'Sorry, user cannot be logged out'
              ], Response::HTTP_INTERNAL_SERVER_ERROR);
          }
      }

      public function get_user(Request $request)
      {
          $this->validate($request, [
              'token' => 'required'
          ]);

          $user = JWTAuth::authenticate($request->token);

          return response()->json(['user' => $user]);
      }

}
