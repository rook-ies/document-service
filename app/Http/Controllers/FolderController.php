<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use App\Models\Folder;
use App\Models\Document;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class FolderController extends Controller
{

      protected $user;

      public function __construct()
      {
          $this->user = JWTAuth::parseToken()->authenticate();
      }

    public function index()
    {
      $folder = Folder::where('is_public', true)
                ->orWhere('owner_id',auth()->user()->id)
                ->orWhere('share',auth()->user()->id)
                ->get();

      return response()->json([
      "success" => true,
      "message" => "Folder List",
      "data" => $folder
      ]);
    }

    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'type' => 'required',
            'is_public' => 'required',
        ]);

        $folder = Folder::updateOrCreate(
            ['_id'          => $request['id']],
            ['name'          => $request['name'],
            'type'       => $request['type'],
            'is_public'   => $request['is_public'],
            'owner_id'       =>  auth()->user()->id,
            'share'        => $request['share'],
            'company_id'       => auth()->user()->company_id,
            'timestamp'       => date('Y-m-d')]
        );

        return response()->json([
        "success" => true,
        "message" => "Success set folder",
        "data" => $folder
        ]);

    }

    public function show($id)
    {
        $document = Document::where('folder_id', $id)->get();
        return response()->json([
        "success" => true,
        "message" => "List file per folder",
        "data" => $document
        ]);
    }

    public function destroy($id)
    {
        $folder = Folder::find($id);
        $folder->delete();
        return response()->json([ "success" => true,
        "message" => "folder deleted successfully.", "data" => $folder]);
    }
}
