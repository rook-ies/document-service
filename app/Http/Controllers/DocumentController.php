<?php

namespace App\Http\Controllers;

use JWTAuth;
use Illuminate\Http\Request;
use App\Models\Document;
use Tymon\JWTAuth\Exceptions\JWTException;
use Symfony\Component\HttpFoundation\Response;
use Illuminate\Support\Facades\Validator;

class DocumentController extends Controller
{

    public function index()
    {
        //
    }

    public function store(Request $request)
    {
      $this->validate($request, [
          'name' => 'required',
          'type' => 'required',
      ]);

      $document = Document::updateOrCreate(
          ['_id'          => $request['id']],
          ['name'          => $request['name'],
          'type'       => $request['type'],
          'folder_id'       => $request['folder_id'],
          'content'   => $request['content'],
          'owner_id'       =>  auth()->user()->id,
          'share'        => $request['share'],
          'company_id'       => auth()->user()->company_id,
          'timestamp'       => date('Y-m-d')]
      );

      return response()->json([
          "success" => true,
          "message" => "Success set document",
          "data" => $document
      ]);
    }

    public function show($id)
    {
        $document = Document::where('_id', $id)->get();
        return response()->json([ "success" => true, "data" => $document
        ]);
    }

    public function destroy($id)
    {
      $document = Document::find($id);
      $document->delete();
      return response()->json([
      "success" => true, "message" => "document deleted successfully.",
      "data" => $document
      ]);
    }
}
